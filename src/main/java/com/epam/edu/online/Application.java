package com.epam.edu.online;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

    private static final Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        for (int i = 0; i <1; i++) {
            LOGGER.debug("This is a debug message - " + i);
            LOGGER.info("This is a info message - " + i);
            LOGGER.warn("This is a warn message - " + i);
            LOGGER.error("This is a error message - " + i);
            LOGGER.fatal("This is a fatal message - " + i);
        }


    }
}
